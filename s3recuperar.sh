#!/bin/bash

bucket=$1
prefix="Flex III y IV-dpto.admin.yfinanzas/Impuestos"

set -e

echo "Removing all latest delete markers in $bucket with prefix $prefix"

#versions=`aws s3api list-object-versions --bucket $bucket | jq '.Versions | .[] | select(.IsLatest | not)'`
latest_delete_markers=`aws s3api list-object-versions --bucket $bucket --prefix "$prefix" | jq '.DeleteMarkers | .[] | select(.IsLatest)'`

 echo "latest delete markers:"
 echo "$latest_delete_markers"

 echo "removing delete markers"
for marker in $(echo "${latest_delete_markers}" | jq -r '@base64'); do
    marker=$(echo ${marker} | base64 --decode)

     echo "marker:"
     echo "$marker"

    key=`echo $marker | jq -r .Key`
    versionId=`echo $marker | jq -r .VersionId `

    printf "Removing delete marker $key $versionId ... "

    aws s3api delete-object --bucket $bucket --key "$key" --version-id $versionId > /dev/null
    printf "✓\n"
    
     echo $cmd
     $cmd
done
